<?php
namespace App;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class App {
  public function run(Request $request)
  {
    $response = new Response(
      'I1 - Host:' . $request->getHost(),
      Response::HTTP_OK,
      ['content-type' => 'text/html']
    );
    $response->send();
  }
}
