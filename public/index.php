<?php
require('../vendor/autoload.php');

use Symfony\Component\HttpFoundation\Request;
use App\App;

$request = Request::createFromGlobals();

$app = new App();
$app->run($request);
