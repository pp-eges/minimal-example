#!/bin/bash
DOMAINKEY=${1:-master}
cp docker-compose.override.yml.ci docker-compose.override.yml
ID=$DOMAINKEY docker-compose -p "$DOMAINKEY-minimal-example" down
ID=$DOMAINKEY docker-compose -p "$DOMAINKEY-minimal-example" build
ID=$DOMAINKEY docker-compose -p "$DOMAINKEY-minimal-example" up -d
echo "http://dev$DOMAINKEY.dev.wdh.hu"
