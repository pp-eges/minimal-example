#!/bin/bash
DOMAINKEY=${1:-master}
cp docker-compose.override.yml.ci docker-compose.override.yml
ID=$DOMAINKEY docker-compose -p "$DOMAINKEY-minimal-example" down
