FROM tanarurkerem/laravel:dev as dev
COPY composer* /project/
RUN composer install
COPY . /project/

FROM tanarurkerem/laravel:dev as build
COPY composer* /project/
RUN composer install --no-dev
COPY . /project

FROM tanarurkerem/laravel
COPY --from=build /project /project

